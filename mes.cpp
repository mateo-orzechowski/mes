#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "matrix.h"
#include "mes.h"

using namespace std;

double get_c(const Element element)
{
    return element.k * element.s / element.lenght;
}

int elements_count(const GlobalData globalData)
{
    return globalData.nodes_count - 1;
}

double element_length(const GlobalData globalData)
{
    return globalData.lenght / elements_count(globalData);
}

Element *generate_elements(const GlobalData globalData)
{
    cout << "Generowanie elementwow...";

    Element *elements = new Element[elements_count(globalData)];

    for (int i = 0; i < elements_count(globalData); i++)
    {
        Element e;

        e.id[0] = i;
        e.id[1] = i+1;

        e.k = globalData.k;
        e.s = globalData.s;
        e.lenght = element_length(globalData);

        elements[i] = e;
    }

    cout << "\t\t\t[OK]" << endl;

    return elements;
}

void fill_h(Element * elements, const GlobalData globalData)
{
    cout << "Wypelnianie macierzy H elementwow...";

    for (int i = 0; i < elements_count(globalData); i++)
    {
        const double c = get_c(elements[i]);

        // cout << endl<< "\t Element " << i << ": c = K * S / L = " << e.k << " * " << e.s << " / " << e.lenght << " = " << c << endl;

        elements[i].h[0][0] =  c;
        elements[i].h[0][1] = -c;

        elements[i].h[1][0] = -c;
        elements[i].h[1][1] =  c;
    }

    cout << "\t\t[OK]" << endl;
}

void fill_hbc(Element * elements, const GlobalData globalData)
{
    cout << "Wypelnianie macierzy HBC elementwow...";

    BC *nodes = globalData.nodes;

    for (int i = 0; i < elements_count(globalData); i++)
    {
        elements[i].hbc[0][0] = 0;
        elements[i].hbc[0][1] = 0;

        elements[i].hbc[1][0] = 0;
        elements[i].hbc[1][1] = 0;

        if (nodes[elements[i].id[0]] == BC::convection)
        {
            elements[i].hbc[0][0] = globalData.alfa * globalData.s;
        }

        if (nodes[elements[i].id[1]] == BC::convection)
        {
            elements[i].hbc[1][1] = globalData.alfa * globalData.s;
        }
    }

    cout << "\t\t[OK]" << endl;
}

void fill_p(Element * elements, const GlobalData globalData)
{
    cout << "Wypelnianie macierzy P elementwow...";

    BC *nodes = globalData.nodes;

    for (int i = 0; i < elements_count(globalData); i++)
    {
        elements[i].p[0] = 0;
        elements[i].p[1] = 0;

        if (nodes[elements[i].id[0]] == BC::convection)
        {
            elements[i].p[0] = -1 * globalData.alfa * globalData.s * globalData.t;
        }

        if (nodes[elements[i].id[0]] == BC::conduction)
        {
            elements[i].p[0] = globalData.q * globalData.s;
        }

        if (nodes[elements[i].id[1]] == BC::convection)
        {
            elements[i].p[1] = -1 * globalData.alfa * globalData.s * globalData.t;
        }

        if (nodes[elements[i].id[1]] == BC::conduction)
        {
            elements[i].p[1] = globalData.q * globalData.s;
        }
    }

    cout << "\t\t[OK]" << endl;
}

double** get_global_h(const Element * elements, const GlobalData globalData)
{
    cout << "Skladanie globalnej macierzy H...";

    const int global_matrix_size = globalData.nodes_count;

    double** global_h = new double* [global_matrix_size];

    for (int i = 0; i < global_matrix_size; i++)
    {
        global_h[i] = new double[global_matrix_size];

        for (int j = 0; j < global_matrix_size; j++)
        {
            global_h[i][j] = 0;
        }
    }

    for (int i = 0; i < global_matrix_size - 1; i++)
    {
        Element e = elements[i];

        global_h[e.id[0]][e.id[0]] += e.h[0][0] + e.hbc[0][0];
        global_h[e.id[0]][e.id[1]] += e.h[0][1] + e.hbc[0][1];
        global_h[e.id[1]][e.id[0]] += e.h[1][0] + e.hbc[1][0];
        global_h[e.id[1]][e.id[1]] += e.h[1][1] + e.hbc[1][1];
    }

    cout << "\t\t[OK]" << endl;

    return global_h;
}

double* get_global_p(const Element * elements, const GlobalData globalData)
{
    cout << "Skladanie globalnej macierzy P...";

    const int global_matrix_size = globalData.nodes_count;

    double* global_p = new double [global_matrix_size];

    for (int i = 0; i < global_matrix_size; i++)
    {
        global_p[i] = 0;
    }

    for (int i = 0; i < global_matrix_size - 1; i++)
    {
        Element e = elements[i];

        global_p[i]   += e.p[0];
        global_p[i+1] += e.p[1];
    }

    cout << "\t\t[OK]" << endl;

    return global_p;
}

void print(double** global_h, double* global_p, double* temperatures, const GlobalData globalData)
{
    cout << "Dane:" << endl << endl;
    cout << "K: " << globalData.k << endl;
    cout << "S: " << globalData.s << endl;
    cout << "dlugosc calego elementu: " << globalData.lenght << endl;
    cout << "alfa: " << globalData.alfa << endl;
    cout << "temperatura otoczenia: " << globalData.t << endl;
    cout << "q: " << globalData.q << endl;
    cout << "ilosc elementow: " << globalData.nodes_count << endl;
    cout << "warunki brzegowe wezlow: ";
    for (int i = 0; i < globalData.nodes_count; i++)
    {
        switch (globalData.nodes[i])
        {
            case BC::none:
                cout << "-";
                break;
            case BC::conduction:
                cout << "przwodzenie";
                break;
            case BC::convection:
                cout << "konwekcja";
                break;
        }

        cout << " ";
    }
    cout << endl;

    cout << endl << "-----------------------------------" << endl << endl;

    cout << "Globalna macierz H:" << endl << endl;

    for (int i = 0; i < globalData.nodes_count; i++)
    {
        for (int j = 0; j < globalData.nodes_count; j++)
        {
            cout << global_h[i][j] << "\t";
        }

        cout << endl;
    }

    cout << endl << "Globalna macierz P:" << endl << endl;

    for (int i = 0; i < globalData.nodes_count; i++)
    {
        cout << global_p[i] << endl;
    }

    cout << endl << "Temperatury:" << endl << endl;

    for (int i = 0; i < globalData.nodes_count; i++)
    {
        cout << temperatures[i] << endl;
    }
}

double** inverse_h(double** global_h, const GlobalData globalData)
{
    cout << "Odwracanie globalnej macierzy H...";

    CMatrix h("H", globalData.nodes_count, globalData.nodes_count);
    h.SetValues(global_h);

    // cout << endl; h.Inverse().PrintRaw();

    cout << "\t\t[OK]" << endl;

    return h.Inverse().Values();
}

double* calculate_temperatures(double** inversed_global_h, double* global_p, const GlobalData globalData)
{
    cout << "Obliczanie temperatur dla wezlow...";

    double* tempratures = new double[globalData.nodes_count];

    // cout << endl;

    for (int i = 0; i < globalData.nodes_count; i++)
    {
        tempratures[i] = 0.0;
        // cout << "t = " << tempratures[i] << ";\t";
        for (int j = 0; j < globalData.nodes_count; j++)
        {
            // cout << "t = t + Hij x Pj  = " << tempratures[i] << " + ( " << inversed_global_h[i][j] << " * " << global_p[j] << " * -1 ) = " <<  tempratures[i] << " + " << inversed_global_h[i][j] * global_p[j] * -1 << " = " << inversed_global_h[i][j] * global_p[j] * -1 + tempratures[i] << endl;

            tempratures[i] += inversed_global_h[i][j] * global_p[j] * -1;
        }
    }

    cout << "\t\t[OK]" << endl;

    return tempratures;
}
/* File structure
 * k                     double
 * s                     double
 * whole element length  double
 * alfa                  double
 * ambient temperature   double
 * q                     double
 * number of nodes       int
 * nodes BCs             int[]     convection - 2, conduction - 1, none - 0
 */
GlobalData get_data_from_file(string filename)
{
    cout << "Pobieranie danych z pliku...";
	GlobalData data;

	fstream file;
	file.open(filename);

	if (!file.good())
    {
        throw "file could not be opened";
	}

	stringstream ss;

	ss << file.rdbuf();
    ss >> data.k;
    ss >> data.s;
    ss >> data.lenght;
    ss >> data.alfa;
    ss >> data.t;
    ss >> data.q;
    ss >> data.nodes_count;

    data.nodes = new BC[data.nodes_count];

    for (int i = 0; i < data.nodes_count; i++)
    {
        int node_bc;
        ss >> node_bc;

        switch (node_bc)
        {
            case 0:
                data.nodes[i] = BC::none;
                break;
            case 1:
                data.nodes[i] = BC::conduction;
                break;
            case 2:
                data.nodes[i] = BC::convection;
                break;
        }
    }

    cout << "\t\t\t[OK]" << endl;

	return data;
}

