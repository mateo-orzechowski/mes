#ifndef MES_H_INCLUDED
#define MES_H_INCLUDED

#include <string>

using namespace std;

enum BC {
    none,
    conduction,
    convection,
};

struct Element {
    int id[2];
    double h[2][2];
    double hbc[2][2];
    double p[2];
    double k;
    double s;
    double lenght;
};

struct GlobalData {
    double lenght;
    double k;
    double s;
    double q;
    double alfa;
    double t;
    int nodes_count;
    BC *nodes = nullptr;
};

double element_length(const GlobalData);
int elements_count(const GlobalData);
Element *generate_elements(const GlobalData);

double get_c(const Element);

void fill_h(Element *, const GlobalData);
void fill_hbc(Element *, const GlobalData);
void fill_p(Element *, const GlobalData);

double** get_global_h(const Element *, const GlobalData);
double*  get_global_p(const Element *, const GlobalData);

void print(double**, double*, double*, const GlobalData);

double** inverse_h(double**, const GlobalData);
double*  calculate_temperatures(double**, double*, const GlobalData);

GlobalData get_data_from_file(string);

#endif // MES_H_INCLUDED
