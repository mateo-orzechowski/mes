#include <iostream>

#include "mes.h"

using namespace std;

GlobalData mock_data(int nodes = 3)
{
    GlobalData gd;

    gd.k = 50;
    gd.s = 2;
    gd.lenght = 5;
    gd.alfa = 10;
    gd.t = 400;
    gd.q = -150;
    gd.nodes_count = nodes;
    gd.nodes = new BC[nodes];

    gd.nodes[0] = BC::convection;
    for (int i = 1; i < nodes - 1; i++)
    {
        gd.nodes[i] = BC::none;
    }
    gd.nodes[nodes-1] = BC::conduction;

    return gd;
}

int main()
{
    GlobalData gd;
    gd = get_data_from_file("data.txt");
    //gd = mock_data(6);

    Element *elements = generate_elements(gd);

    fill_h(elements, gd);
    fill_hbc(elements, gd);
    fill_p(elements, gd);

    double** global_h = get_global_h(elements, gd);
    double** inversed_global_h = inverse_h(global_h, gd);

    double*  global_p = get_global_p(elements, gd);

    double* temperatures = calculate_temperatures(inversed_global_h, global_p, gd);

    cout << endl << "-----------------------------------" << endl << endl;

    print(global_h, global_p, temperatures, gd);

    delete gd.nodes;
    delete [] global_h;
    delete [] inversed_global_h;
    delete global_p;
    delete temperatures;

    return 0;
}

